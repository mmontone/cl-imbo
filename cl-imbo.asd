(asdf:defsystem #:cl-imbo
  :description "Imbo Common Lisp client"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :components ((:file "package")
               (:file "cl-imbo"))
  :depends-on (:local-time
			   :drakma
			   :quri
			   :cl-json
			   :ironclad
			   :trivial-utf-8))
