(in-package #:cl-imbo)

;; Variables

(defvar *imbo-users* nil)
(defvar *imbo-user*)
(defparameter *imbo-baseurl* "http://imbo")
(defparameter +gmt-format+ '((:YEAR 4) #\- (:MONTH 2) #\- (:DAY 2) #\T (:HOUR 2) #\: (:MIN 2) #\: (:SEC 2) #\Z))

;; Macros

(defmacro with-imbo-connection (imbo-baseurl &body body)
  `(call-with-imbo-connection ,imbo-baseurl (lambda () ,@body)))

(defmacro with-imbo-user (user &body body)
  `(call-with-imbo-user ,user (lambda () ,@body)))

(defmacro with-current-imbo-user (user &body body)
  `(let ((,user (or *imbo-user* (error "Call with imbo user"))))
     ,@body))

;; Util

(defun imbo-url (path &rest args)
  (quri:uri (format nil "~A/~A" *imbo-baseurl* (apply #'format nil path args))))

(defun render-uri* (uri)
  (if (typep uri 'quri:uri)
      (quri:render-uri uri)
      (string uri)))

;; Error

(define-condition imbo-error (simple-error)
  ((code :initarg :code
		 :initform (error "Provide the error code")
		 :accessor imbo-error-code)
   (message :initarg :message
			:initform (error "Provide the error message")
			:accessor imbo-error-message)
   (imbo-code :initarg :imbo-code
			  :initform (error "Provide the error imbo code")
			  :accessor imbo-error-imbo-code))
  (:report (lambda (c s)
			 (format s "~A: ~A (~A)"
					 (imbo-error-code c)
					 (imbo-error-message c)
					 (imbo-error-imbo-code c)))))	 

;; Auth

(defun authenticate-url (url imbo-user)
  (push (cons "accessToken" (make-imbo-access-token imbo-user url))
        (quri:uri-query-params url))
  url)

(defun hmac-string (key hash string)
  (let ((hmac (ironclad:make-hmac (trivial-utf-8:string-to-utf-8-bytes key) hash)))
    (ironclad:update-hmac hmac (trivial-utf-8:string-to-utf-8-bytes string))
    (ironclad:byte-array-to-hex-string (ironclad:hmac-digest hmac))))

(defun make-imbo-access-token (user uri)
  (hmac-string (getf user :private-key) 'ironclad:sha256 (render-uri* uri)))

(defun imbo-auth-headers (http-method url imbo-user)
  (let* ((timestring (local-time:format-timestring nil (local-time:now) :format +gmt-format+ :timezone (local-time:find-timezone-by-location-name "GMT")))
         (data (format nil "~A|~A|~A|~A"
                       (string-upcase (princ-to-string http-method))
                       url
                       (getf imbo-user :username)
                       timestring))
         (signature (hmac-string (getf imbo-user :private-key) 'ironclad:sha256 data)))
    (list (cons "X-Imbo-Authenticate-Signature" signature)
          (cons "X-Imbo-Authenticate-Timestamp" timestring))))

;; Transformations encoding

(defun encode-transformations (uri transformations)
  (loop for transformation in transformations
     do
       (push (cons "t[]" (encode-transformation transformation))
             (quri:uri-query-params uri)))
  uri)

(defun encode-transformation (transformation)
  (with-output-to-string (s)
    (write-string
     (json:lisp-to-camel-case
      (string
       (or (and (not (listp transformation))
                transformation)
           (first transformation))))
     s)
    (when (and (listp transformation) (rest transformation))
      (format s ":")
	  (format s "~{~A~^,~}"
			  (mapcar (lambda (arg-and-value)
						(format nil "~A=~A" 
								(json:lisp-to-camel-case
								 (string 
								  (car arg-and-value)))
								(cdr arg-and-value)))
					  (alexandria:plist-alist (rest transformation)))))))

;; Connection

(defun imbo-connect (imbo-baseurl)
  (setf *imbo-baseurl* imbo-baseurl))

(defun imbo-disconnect ()
  (setf *imbo-baseurl* nil))

(defun call-with-imbo-connection (imbo-baseurl function)
  (let ((*imbo-baseurl* imbo-baseurl))
	(funcall function)))


;; User

(defun call-with-imbo-user (user function)
  (let ((imbo-user (or (and (stringp user)
                            (find-imbo-user user))
                       (and (listp user)
                            user)
                       (error "Bad imbo user: ~S" user))))
    (let ((*imbo-user* imbo-user))
      (funcall function))))

(defun define-imbo-user (username private-key)
  (push (list :username username
              :private-key private-key)
        *imbo-users*)
  t)

(defun find-imbo-user (username &key (error-p t))
  (or
   (find username *imbo-users* :test #'string= :key (lambda (user-info)
                                                      (getf user-info :username)))
   (when error-p
     (error "Imbo user ~s not defined" username))))

(defun use-imbo-user (user)
  (setf *imbo-user* (or (and (stringp user)
							 (find-imbo-user user))
						(and (listp user)
							 user)
						(error "Bad imbo user: ~S" user))))

;; Client functions

(defun imbo-root ()
  (json:decode-json-from-string
   (babel:octets-to-string
	(drakma:http-request (render-uri* (imbo-url ""))))))

(defun get-user-info ()
  (with-current-imbo-user imbo-user
    (json:decode-json-from-string
	 (babel:octets-to-string
	  (drakma:http-request
	   (render-uri*
		(authenticate-url (imbo-url "users/~A.json"
									(getf imbo-user :username))
						  imbo-user)))))))

(defun imbo-status ()
  (json:decode-json-from-string
   (babel:octets-to-string
	(drakma:http-request (render-uri* (imbo-url "status.json"))))))

(defun imbo-stats ()
  (json:decode-json-from-string
   (babel:octets-to-string
	(drakma:http-request (render-uri* (imbo-url "stats.json"))))))

(defmethod add-image ((content pathname) &optional (error-p t))
  (add-image (alexandria:read-file-into-byte-vector content)
             error-p))
                      
(defmethod add-image ((content vector) &optional (error-p t))
  (let ((output
		 (with-current-imbo-user imbo-user
		   (let ((url (imbo-url "users/~A/images" (getf imbo-user :username))))
			 (json:decode-json-from-string
			  (babel:octets-to-string
			   (drakma:http-request (render-uri* url)
									:method :post
									:content content
									:additional-headers
									(imbo-auth-headers :post (render-uri* url) imbo-user))))))))
	(if error-p
		(if (assoc :error output)
			(let ((imbo-error (cdr (assoc :error output))))
			  (error 'imbo-error
					 :code (cdr  (assoc :code imbo-error))
					 :message (cdr (assoc :message imbo-error))
					 :imbo-code (cdr (assoc :imbo-error-code imbo-error))))
			output)
		output)))

(defun get-image-url (id &rest transformations)
  (render-uri*
   (with-current-imbo-user imbo-user
	 (authenticate-url
	  (encode-transformations (imbo-url "users/~A/images/~A"
										(getf imbo-user :username)
										id)
							  transformations)
	  imbo-user))))

(defun get-image (id &rest transformations)
  (drakma:http-request (render-uri* (apply #'get-image-url id transformations))))

(defun delete-image (id)
  (with-current-imbo-user imbo-user
    (let ((url (imbo-url "users/~A/images/~A"
                         (getf imbo-user :username)
                         id)))
      (json:decode-json-from-string
       (babel:octets-to-string
        (drakma:http-request
		 (render-uri* url)
		 :method :delete
		 :additional-headers (imbo-auth-headers :delete (render-uri* url) imbo-user)))))))
