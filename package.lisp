(defpackage #:cl-imbo
  (:nicknames :imbo)
  (:use #:cl)
  (:export #:*imbo-user*
		   #:imbo-connect
		   #:imbo-disconnect
		   #:with-imbo-connection
		   #:define-imbo-user
		   #:use-imbo-user
		   #:find-imbo-user
		   #:with-imbo-user
		   #:with-current-imbo-user
		   #:imbo-root
		   #:get-user-info
		   #:imbo-status
		   #:imbo-stats
		   #:add-image
		   #:get-image
		   #:get-image-url
		   #:delete-image
		   #:imbo-error))

